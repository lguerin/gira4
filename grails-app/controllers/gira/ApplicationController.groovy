package gira

import grails.core.GrailsApplication
import grails.plugins.*

class ApplicationController implements PluginManagerAware {

    GrailsApplication grailsApplication
    GrailsPluginManager pluginManager

    def index() {
        User firstUser = User.list()?.get(0)
        [grailsApplication: grailsApplication, pluginManager: pluginManager, user: firstUser]
    }
}
