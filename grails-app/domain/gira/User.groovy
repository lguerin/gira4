package gira

class User {

    String login
    String fullname
    String email

    static constraints = {
        login nullable: false, blank: false
        fullname nullable: false, blank: false
        email nullable: true, blank: false, email: true
    }

    static mapping = {
        table name: '`user`'
        id name: 'login', generator: 'assigned'
        login unique: true
        version false
        cache true
    }
}
