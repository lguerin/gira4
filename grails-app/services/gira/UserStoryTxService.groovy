package gira


import grails.gorm.transactions.Transactional

@Transactional
class UserStoryTxService {

    @Transactional
    boolean updateStoryPointsFromTx2(Serializable id, int points, int latency) {
        log.info "Update user story points from UserStoryTxService: $points points"
        UserStory us = UserStory.get(id)
        us.storyPoints = points
        Thread.sleep(latency)
        return true
    }
}
