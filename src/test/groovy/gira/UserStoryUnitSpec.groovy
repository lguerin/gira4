package gira

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class UserStoryUnitSpec extends Specification implements DomainUnitTest<UserStory> {

    def setup() {
    }

    def cleanup() {
    }

    void "should persist user story instances"() {
        setup:
        Fixtures.buildUserStory()
        Fixtures.buildUserStory()

        expect:
        UserStory.count() == 2
    }

    void "should update story points"() {
        setup:
        Fixtures.buildUserStory([title: 'us1'])

        when:
        UserStory us1 = UserStory.findByTitle('us1')
        us1.storyPoints = 20
        us1.save()

        then:
        UserStory.findByTitle('us1')?.storyPoints == 20
    }
}
